[![pipeline status](https://gitlab.com/KAction/hsymbol/badges/master/pipeline.svg)](https://gitlab.com/KAction/hsymbol/commits/master)

Automatic explicit imports for Haskell
======================================

It is controversal topic, whether explicit imports, like this

    import Data.Maybe (maybe)

is good thing or bad. Advocates highlight possible code breakages on
minor dependencies upgrade, opponents like to remind about git merge
conflicts. But one fact is not controversal – writing explicit imports
is boring and mechanical. Never send human to do a job of machine!

The `hsymbol` tool builds database, containing information, which
modules provide given function, restricting search to dependencies
of your project and provides interface for quering that database.

Using beautiful [hsimport](https://hackage.haskell.org/package/hsimport)
tool, it is possible to programmatically add required qualified import.
Actually, in query mode, `hsymbol` outputs information in format,
suitable for forwarding to `hsimport` program. There is also basic
Vim integration script.

The following programs are required for `hsymbol` to function properly:

 * [codex](https://hackage.haskell.org/package/codex) — tool for
   creating tags file for project and all its dependencies.

 * [hsimport](https://hackage.haskell.org/package/hsimport) — tool for
   programmatically modifying import lines in Haskell sources.

Limitation
----------

There are two know issues with current implementation. First, some
symbols are meant to be imported explicitly, like `maybe`; some symbols
are meant to be accessed with qualified imports, like `count` from
`Data.Text`. Currently, list of modules that should be imported
qualified just hard-coded in binary.

Another issue is that tags files countais refrences to internal modules,
making `hsymbol` output more options, than actually possible. This
problem is softened by ugly special case, inhibiting suggestions
of modules, containing string `Internal` string in their name.

Probably, parsing haddock files directly could provide more precise
suggestions.

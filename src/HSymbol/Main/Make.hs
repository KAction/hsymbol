{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
module HSymbol.Main.Make (main) where
import Data.Conduit (ConduitT, runConduit, (.|), awaitForever, yield, await)
import Data.ByteString (ByteString)
import qualified Data.Conduit.Combinators as CC
import qualified Data.ByteString.Char8    as C8
import qualified Data.ByteString          as B
import Data.Char (isUpper, isAlpha)
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Store (encode)
import HSymbol.Types (Import(..), Tag, Storage)

-- Symbols from these modules are known to be meant imported qualified
-- Mapping: Module -> (prefix, qualify-types?)
specialModules :: [(ByteString, (ByteString, Bool))]
specialModules =
  [ ("Data.ByteString", ("BS", False))
  , ("Data.ByteString.Char8", ("C8", True))
  ]

startsWithUpper :: ByteString -> Bool
startsWithUpper bs =
  case C8.uncons bs of
    Nothing -> False
    Just (c, _) -> isUpper c

processTags :: ConduitT ByteString (Tag, Import) IO ()
processTags = awaitForever $ \line ->
  case C8.split '\t' line of
    (tag:filename:_lineno:_) -> do
      let mod' = B.intercalate "."
               . filter startsWithUpper
               . filter (/= "Hackage") -- ugly hack
               . C8.split '/'
               . C8.take (C8.length filename - 3)
               $ filename
      if tag `elem` ["(", "{", ")", ""]
      then pure ()
      else do
        let tag' = if C8.head tag == '!'
                   then C8.drop 1 tag
                   else tag
            tag'' = if C8.head tag == '('
                    then C8.drop 1 . C8.init $ tag'
                    else tag'
        if tag'' == B.empty || (B.length tag'' == 1 && isAlpha (C8.head tag''))
        then pure ()
        else
          case lookup mod' specialModules of
            Just (prefix, incT) ->
              if not incT && isUpper (C8.head tag'')
              then pure ()
              else yield (prefix <> "." <> tag'', Import mod' (Just prefix))
            Nothing -> yield (tag'', Import mod' Nothing)
    _ -> pure ()

sinkMap :: (Monad m, Ord k, Ord v)
        => ConduitT (k, v) a m (Map k (Set v))
sinkMap = go Map.empty
  where
    go m = await >>= \case
      Just (k, v) -> go $ Map.insertWith Set.union k (Set.singleton v) m
      Nothing     -> pure m

main :: IO ()
main = do
  let _ = processTags
  m <- runConduit $ CC.stdin
                 .| CC.linesUnboundedAscii
                 .| processTags
                 .| sinkMap
  C8.putStr . encode $ (m :: Storage)
